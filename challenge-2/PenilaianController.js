/**
 * @author Ari Ardiansyah
 * @package binar-academy (kampus-merdeka)
 * @description challenge-2
 */
const BaseController = require("./BaseController");
const Nilai = require("./Nilai");
 
class PenilaianController extends BaseController {

    constructor(){
        super()
        this.mNilai = new Nilai()
    }

    showQuestion(){
        console.log('')
        console.log('🖥️  Penilaian siswa')
        console.log('---------------------------------------')
        console.log('\x1b[33m%s\x1b[0m', 'Silakan inputkan nilai dan ketik "q" jika sudah selesai!\x1b[0m')
        console.log('')
        
        const inputNilai = () => this.readLine().question('Nilai: ', (answer) => {
            answer = answer.trim()
            if(answer == 'q'){
                this.#stopQuestion()
                return;
            }

            let ans = answer
            answer = Number(answer)

            if((ans != '') && !isNaN(answer) && (answer != null) && (typeof(answer) == 'number')) {
                this.mNilai.addNilai(answer)
                inputNilai()
            }else{
                console.log('\x1b[33m%s\x1b[0m', "🛈 Silakan masukan angka (number) atau \"q\"\x1b[0m")
                inputNilai()
            }
        })

        inputNilai()
    }

    /** Private */
    #stopQuestion(){
        console.log('')
        if(this.mNilai.getCountNilai() > 0){
            console.log('Selesai memasukan nilai, cari output: ')
            console.log('')
            console.log('1. Nilai tertinggi & terendah')
            console.log(`\x1b[32m✔ Nilai tertinggi: ${this.mNilai.getNilaiTertinggi()}\x1b[0m`)
            console.log(`\x1b[32m✔ Nilai terendah: ${this.mNilai.getNilaiTerendah()}\x1b[0m`)
            console.log(`\x1b[32m✔ Urutan nilai dari terendah ke tertinggi:\x1b[0m`)
            console.table(this.mNilai.getSortedNilai())
            console.log('')
            console.log('2. Mencari rata-rata')
            console.log(`\x1b[32m✔ Rata-rata nilai: ${this.mNilai.getSumNilai()} / ${this.mNilai.getCountNilai()} = ${this.mNilai.getNilaiRataRata()}\x1b[0m`)
            console.log('')
            console.log('3. Jumlah siswa lulus & tidak lulus')
            console.log(`\x1b[32m✔ KKM: ${this.mNilai.kkm}\x1b[0m`)
            console.log(`\x1b[32m✔ Jumlah siswa lulus: ${this.mNilai.getSiswaLulus().length}\x1b[0m`)
            console.log(`\x1b[32m✔ Jumlah siswa tidak lulus: ${this.mNilai.getSiswaTidakLulus().length}\x1b[0m`)
            console.log('')
        }else{
            console.log("\x1b[31m❌ Penilaian Kosong\x1b[0m")
            console.log('')
        }

        //Need back?
        this.#askForTryAgain()

    }

    /** Private */
    #askForTryAgain(){
        this.readLine().question('Kembali ke awal (y/t)? ', (answer) => {
            if(answer.toLocaleLowerCase() == 'y'){
                this.mNilai.nilaiSiswa = []
                this.showQuestion()
            }else{
                this.readLine().close()
            }
        })
    }
}

module.exports = PenilaianController