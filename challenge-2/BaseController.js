/**
 * @author Ari Ardiansyah
 * @package binar-academy (kampus-merdeka)
 * @description challenge-2
 */
 const readline = require('readline');

 class BaseController {
     constructor(){
         this.rl = readline.createInterface({
             input: process.stdin,
             output: process.stdout
         });
         
         this.rl.on('close', function () {
             console.log('\nTerima kasih, telah menggunakan 😘');
             process.exit(0);
         });
     }
     
     readLine() {
         return this.rl
     }
 }
 
 module.exports = BaseController