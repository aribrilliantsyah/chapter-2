/**
 * @author Ari Ardiansyah
 * @package binar-academy (kampus-merdeka)
 * @description challenge-2
 */
 class Nilai {
    nilaiSiswa = []
    kkm = 75

    constructor(nilaiSiswa, kkm){
        this.nilaiSiswa = (nilaiSiswa != undefined && Array.isArray(nilaiSiswa)) ? nilaiSiswa : []
        this.kkm = kkm || 75
    }

    addNilai(nilai){
        this.nilaiSiswa.push(nilai)
    }

    getNilai(){
        return this.nilaiSiswa
    }

    getSortedNilai(){
        const bubleSort = (arr) => {
            var len = arr.length
            for(var i = len-1; i>=0; i--){
                for(var j = 1; j<=i; j++){
                    if(arr[j-1]>arr[j]){
                        var temp = arr[j-1]
                        arr[j-1] = arr[j]
                        arr[j] = temp
                    }
                }
            }
            return arr;
        }

        return bubleSort(this.nilaiSiswa)
    }
    
    getNilaiTertinggi(){
        return this.getSortedNilai()[this.nilaiSiswa.length - 1]
    }

    getNilaiTerendah(){
        return this.getSortedNilai()[0]
    }

    getSiswaLulus(){
        return this.nilaiSiswa.filter((val) => {
            return val >= this.kkm
        })
    }
    
    getSiswaTidakLulus(){
        return this.nilaiSiswa.filter((val) => {
            return val < this.kkm
        })
    }

    getSumNilai(){
        let data = this.nilaiSiswa
        let sum = 0

        data.forEach((nilai) => {
            sum += nilai
        })

        return sum
    }

    getCountNilai(){
        return this.nilaiSiswa.length
    }

    getNilaiRataRata(){
        return Math.round(this.getSumNilai() / this.getCountNilai())
    }   
}

module.exports = Nilai