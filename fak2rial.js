function computeFactorial(num) {
    let results = 1

    for(let i = 2; i <= num; i++) {
        results = results * i
    }

    return results
}

console.log(computeFactorial(8))

console.log('--------------------')

function calculateFactorial(num) {
    console.log(typeof num)
    if(num === 1){
        return 1
    }else{
        return num * calculateFactorial(num-1);
    }
}

// console.log(calculateFactorial(" 8 "))
