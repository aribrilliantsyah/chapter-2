const { JSDOM } = require("jsdom")
const { window } = new JSDOM()

// const arr = [3,1,10];
// console.log('panjang data', arr.length);
// console.log(arr[0]) //3
// console.log(arr[1]) //1
// console.log(arr[2]) //10
// console.log(arr[3]) //undefined
// console.log('------')
// console.log(bubbleSort(arr))
// console.log('------')

function bubbleSort(arr){
    var len = arr.length
    console.log(len)
    for(var i = len-1; i>=0; i--){
        console.log(`i: ${i}`)
        // console.log('i2: ', i)
        // console.log('i3: '+ i)
        for(var j = 1; j<=i; j++){
            console.log(`j: ${j}`)
            if(arr[j-1]>arr[j]){
                console.log('----')
                console.log('Swap')
                console.log(`${arr[j-1]} > ${arr[j]}`)
                var temp = arr[j-1]
                console.log(arr)
                arr[j-1] = arr[j]
                console.log(arr)
                arr[j] = temp
                console.log(arr)
                // console.table(arr)
                console.log('----')
            }
        }
    }
    return arr;
}

function bubbleSort2(arr){
    for (let i = arr.length - 1; i > 0; i--) {
        for (let j = 0; j < i; j++) {
            if (arr[j] > arr[j + 1]) {
                // SWAP
                ;[arr[j], arr[j + 1]] = [arr[j + 1], arr[j]]
            }
        }
    }
    return arr
}

const start = window.performance.now()
console.log(bubbleSort2([7,5,10,2,1,11,12,14,50,80,100,111,400,39,482,930,49,22,77,55,52,34]))
const stop = window.performance.now()

console.log(`Time Taken to execute = ${(stop - start)/1000} seconds`);

// console.log(bubbleSort([9,7,5,4,3,1]))
// console.log(bubbleSort([1,2,3,4,5,6]))
// console.log('-------------------------')
// console.log(bubbleSort2([7,5,2,4,3,9]))
// console.log(bubbleSort2([9,7,5,4,3,1]))
// console.log(bubbleSort2([1,2,3,4,5,6]))