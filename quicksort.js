const { JSDOM } = require("jsdom")
const { window } = new JSDOM()

function quickSort(arr, left, right){
    var len = arr.length,
    pivot,
    partitionIndex;

    if(left < right){
        pivot = right
        partitionIndex = partition(arr, pivot, left, right) // 1. 4

        quickSort(arr, left, partitionIndex - 1)
        quickSort(arr, partitionIndex + 1, right)
    }

    return arr
}

function partition(arr, pivot, left, right){
    var pivotValue = arr[pivot],
    partitionIndex = left
    
    for(var i = left; i < right; i++){
        if(arr[i] < pivotValue){
            swap(arr, i, partitionIndex, pivotValue);
            partitionIndex++
        }
    }
    
    swap(arr, right, partitionIndex, pivotValue);
    return partitionIndex
}

function swap(arr, i, j, pivotValue){
    var temp = arr[i]
    arr[i] = arr[j]
    arr[j] = temp
    console.log('pivot', pivotValue)
    console.log(arr.toString())
}

const start = window.performance.now()
// console.log(quickSort([7,5,10,2,1,11,12,14,50,80,100,111,400,39,482,930,49,22,77,55,52,34], 0, 5))
console.log(quickSort([3, 7, 8, 4, 2, 1, 5], 0, 6))
const stop = window.performance.now()

console.log(`Time Taken to execute = ${(stop - start)/1000} seconds`);

