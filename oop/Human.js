class Human {
    static isLivingOnEarth = true;

    constructor(name, address) {
        this.name = name
        this.address = address
    }

    introduce() {
        console.log(`Hi, my name is ${this.name}`)
    }
    work() {
        console.log("Work!")
    }
}

module.exports = Human