
const Human = require('./Human')

class Programmer extends Human {
    constructor(name, address, proggrammingLanguages) {
        super(name, address)
        this.proggrammingLanguages = proggrammingLanguages
    }
    introduce() {
        super.introduce()
        console.log(`I can write`, this.proggrammingLanguages)
    }
    code() {
        let random = Math.floor(Math.random() * this.proggrammingLanguages.length)
        console.log("Code some", this.proggrammingLanguages[random])
    }
}

module.exports = Programmer
