/**
 * @author Ari Ardiansyah
 * @package binar-academy (kampus-merdeka)
 * @description challenge-2
 */
class NilaiSiswa {
    nilaiSiswa = []
    nama = '';
    nilai = 0;

    constructor(nama, nilai){
        this.nama = nama
        this.nilai = nilai
    }

    #idIncrement(){
        let len = this.nilaiSiswa.length;
        return len+1;
    }

    save(){
        this.nilaiSiswa.push({
            id: this.#idIncrement(),
            siswa: this.siswa,
            nilai: this.nilai
        })
    }

    all(){
        return this.nilaiSiswa
    }
    
    find(arr){
        if(!Array.isArray(arr)) throw "Parameter harus Array";
        if(arr.length == 0) return [];
        let new_arr = this.nilaiSiswa.filter((val) => {
            return arr.includes(val.id)
        });

        return new_arr
    }
}

module.exports = NilaiSiswa