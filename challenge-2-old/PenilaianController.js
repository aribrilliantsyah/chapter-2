/**
 * @author Ari Ardiansyah
 * @package binar-academy (kampus-merdeka)
 * @description challenge-2
 */
const BaseController = require("./BaseController");
const NilaiSiswa = require("./NilaiSiswa");

class PenilaianController extends BaseController {

    constructor(){
        super()
        this.nilaiSiswa = new NilaiSiswa()
    }
    
    showQuestion(){
        console.log()
        console.log('🖥️  Penilaian siswa')
        console.log('---------------------------------------')
        console.log('\x1b[33m%s\x1b[0m', 'Silakan ketik sembarang dibagian input siswa dan ketik "q" dibagian input nilai untuk mengakhiri!\x1b[0m')
        
        let siswa = ''
        let nilai = ''

        const startQuestion = () => {
            siswa = ''
            nilai = ''
            
            console.log()
            console.log('---------------------------------------')
            insertNamaSiswa()
        }

        const insertNamaSiswa = () => {
            this.readLine().question('Nama Siswa: ', (answer) => {
                if(answer != ''){
                    siswa = answer
                    insertNilai()
                }else{
                    console.log('\x1b[33m%s\x1b[0m', "🛈 Silakan isi nama\x1b[0m")
                    insertNamaSiswa()
                }
            })
        }

        const insertNilai = () => {
            this.readLine().question('Nilai: ', (answer) => {
                answer = answer.trim()
                if(answer == 'q'){
                    stopQuestion()
                    return;
                }
                let ans = answer
                answer = Number(answer)
                if((ans != '') && !isNaN(answer) && (answer != null) && (typeof(answer) == 'number')) {
                    nilai = answer
                    this.nilaiSiswa.siswa = siswa
                    this.nilaiSiswa.nilai = nilai
                    this.nilaiSiswa.save()
                    startQuestion()
                    // askToLeave()
                }else{
                    console.log('\x1b[33m%s\x1b[0m', "🛈 Silakan masukan angka (number) atau \"q\"\x1b[0m")
                    insertNilai()
                }
            })
        }

        const stopQuestion = () => {
            console.log('')
            console.log('---------------------------------------')
            console.log('Selesai memasukan nilai')
            this.showNilaiSiswa()
        }

        startQuestion()
    }

    showNilaiSiswa(){
        let data = this.nilaiSiswa.all();
        if(data.length > 0){
            console.log('')
            console.log('Tabel nilai siswa')
            console.table(data)
            console.log('')
            this.showNilaiTertinggiTerendah()
            console.log('')
            this.showNilaiRataRata()
            console.log('')
            this.showJumlahLulusTidakLulus()
        }else{
            console.log('')
            console.log("\x1b[31m❌ Penilaian Kosong\x1b[0m")
        }

        this.askForReturn()
    }

    askForReturn(){
        this.readLine().question('Kembali ke awal (y/t)? ', (answer) => {
            if(answer.toLocaleLowerCase() == 'y'){
                this.showQuestion()
            }else{
                rl.close()
            }
        })
    }

    showNilaiTertinggiTerendah(){
        console.log('1. Nilai tertinggi & terendah')
        let nilaiSiswa = this.nilaiSiswa.all()
        nilaiSiswa.sort(function (a, b) {
            return b.nilai - a.nilai;
        })

        console.table(nilaiSiswa)
        console.log('')
        let tertinggi = nilaiSiswa[0]
        let terendah = nilaiSiswa[nilaiSiswa.length - 1]
        
        console.log(`\x1b[32m✔ Nilai Tertinggi: ${tertinggi.siswa} ${tertinggi.nilai}\x1b[0m`)
        console.log(`\x1b[32m✔ Nilai Terendah: ${terendah.siswa} ${terendah.nilai}\x1b[0m`)
    }

    showNilaiRataRata(){
        console.log('2. Nilai rata-rata')
        let nilaiSiswa = this.nilaiSiswa.all()
        let sum = 0;
        let total = 0;

        nilaiSiswa.forEach((item, i) => {
            sum += item.nilai
            total++
        })

        console.log(`\x1b[32m✔ Rata-rata nilai: ${sum} / ${total} = ${Math.floor(sum/total)}\x1b[0m`)
    }

    showJumlahLulusTidakLulus(){
        console.log('3. Jumlah siswa lulus & tidak lulus')
        let nilaiSiswa = this.nilaiSiswa.all()
        let lulus = 0;
        let lulus_arr = [];
        let tidak_lulus = 0;
        let tidak_lulus_arr = [];

        nilaiSiswa.forEach((item, i) => {
            if(item.nilai >= 75){
                lulus++
                lulus_arr.push(item.id)
            }else{
                tidak_lulus++
                tidak_lulus_arr.push(item.id)
            }
        })

        console.log('\x1b[32m✔ KKM: 75\x1b[0m')
        console.log(`\x1b[32m✔ Jumlah siswa lulus: ${lulus}\x1b[0m`)
        console.log(`\x1b[32m✔ Jumlah siswa tidak lulus: ${tidak_lulus}\x1b[0m`)
        console.log('')

        try {
            let siswa_lulus = this.nilaiSiswa.find(lulus_arr)
            let siswa_tidak_lulus = this.nilaiSiswa.find(tidak_lulus_arr)
            console.log('Siswa lulus')
            console.table(siswa_lulus)
            console.log('')
            console.log('Siswa tidak lulus')
            console.table(siswa_tidak_lulus)
            console.log('')
        } catch (error) {
            console.log(error)
        }
    }

}

module.exports = PenilaianController