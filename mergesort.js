const { JSDOM } = require("jsdom")
const { window } = new JSDOM()

function mergeSort(arr) {
    // console.log('arr', arr)
    var len = arr.length
    if(len < 2){
        return arr;
    }

    var mid = Math.floor(len/2),
        left = arr.slice(0, mid),
        right = arr.slice(mid)

    // console.log('left', left)
    // console.log('right', right)

    return merge(mergeSort(left), mergeSort(right))
}

function merge(left, right){
    var result = [],
        lLen = left.length,
        rLen = right.length,
        l = 0,
        r = 0

    while(l < lLen && r < rLen){
        if(left[l] < right[r]){
            let le = left[l++]
            // console.log('l', le)
            result.push(le)
        }else{
            let ri = right[r++];
            // console.log('r', ri)
            result.push(ri)
        }
    }
    // console.log('res', result)
    // console.log('left-slice', left.slice(l))
    // console.log('right-slice', right.slice(r))
    // console.log('concat1', left.slice(l).concat(right.slice(r)) )
    // console.log('concat2', result.concat(left.slice(l)).concat(right.slice(r)))
    return result.concat(left.slice(l)).concat(right.slice(r))
}

const start = window.performance.now()
console.log(mergeSort([7,5,10,2,1,11,12,14,50,80,100,111,400,39,482,930,49,22,77,55,52,34]))
const stop = window.performance.now()

console.log(`Time Taken to execute = ${(stop - start)/1000} seconds`);

// console.log(mergeSort([7,5,2,4,3,9]))