let test = [1,2,3,4,5,6,7,8]

function binarySearch(arr, key, indexof){
    indexof = indexof || null
    if(indexof == null){
        for(var val in arr)
            if(arr[val] === key) indexof = +val
    }

    let len = arr.length
    let low = 0
    let high = len
    let mid = Math.round((len - 1) / 2)
    
    if(len < 2){
        let result = arr[len-1]
        if(result == key){
            return {
                'found': key,
                'index': indexof
            }
        }
        return 'Not Found'
    }

    let midValue = arr[mid]
    if(key < midValue){
        let arr1 = arr.slice(low, mid)
        return binarySearch(arr1, key, indexof)
    }else if(key > midValue){
        let arr1 = arr.slice(mid, high, indexof)
        return binarySearch(arr1, key, indexof)
    }else if(key == midValue){
        let arr1 = arr.slice(mid)
        return binarySearch(arr1, key, indexof)
    }
}

function binarySearch2(arr, key) {
    var len = arr.length,
        low = 0,
        high = len - 1
        
    if(len < 1) {
        return 'Array is empty';
    }

    while (low <= high) {
        let mid = Math.floor((low + high) / 2);
        console.log(`low: ${low}, high: ${high}, mid: ${mid}`);
        if (key < arr[mid]) {
            high = mid - 1;  
        } else if (key > arr[mid]) {
            low = mid + 1;
        } else {
            return "Found at index: " + mid;
        }
    }
    return "Not found"; 
}

// console.log(binarySearch(test, 8))
console.log(binarySearch2(test, 8))