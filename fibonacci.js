const fibonacci = (num) => {
    var a = 1, b = 0, temp

    //1 >= 0 
    while(num >= 0){
        temp = a //1. 1, 2. 2
        a = a + b //1. 1+0, 2. 1+1
        b = temp //1. 1, 2. 2
        num--
    }

    return b
}

console.log(fibonacci(1))