class Human {
    constructor(name, address) {
        this.name = name
        this.address = address
    }

    introduce() {
        console.log(`Hi, my name is ${this.name}`)
    }
    work() {
        console.log("Work!")
    }
}

class Programmer extends Human {
    constructor(name, address, proggrammingLanguages) {
        super(name, address)
        this.proggrammingLanguages = proggrammingLanguages
    }
    introduce() {
        super.introduce()
        console.log(`I can write`, this.proggrammingLanguages)
    }
    code() {
        console.log("Code some", this.proggrammingLanguages[Math.floor(Math.random * this.proggrammingLanguages.length)])
    }
}

let obj = new Programmer('Ari', 'Bandung', ['PHP', 'JS'])
obj.introduce()