const readline = require("readline")
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

rl.on('close', function () {
    console.log('\nTerima kasih, telah menggunakan 😘');
    process.exit(0);
});

class BinaryTree {
    
    constructor(root){
        this.root = root;
        this.index = 0;
    }

    treeView(search){
        search = search || '';
        const render_space = (length) => {
            let str = '';
            if(length > 0){
                for(let i = 0; i < length; i++){
                    str += ' ';
                }
            }
            return str;
        }

        const render_tree = (root, index) => {
            let space = 4 * index;
            if(index == 0) {
                if(root.key == search){
                    console.log(`\x1b[32m${String(root?.key)}\x1b[0m`)
                }else{
                    console.log(String(root?.key))
                }
            }    
            let seperator_left = (root.right?.key == null ? '└── ': '├── ');
            let seperator_right = '└── ';
            
            let prefix_left = '';
            let prefix_right = '';
            let space_left = space;
            let space_right = space;
            if(index > 0 && root.right?.key != null){
                prefix_left = '│'
                space_left = space - 1
            }
            if(index > 0 && root.left?.key != null){
                prefix_right = '│'
                space_right = space - 1
            }

            if(root.left?.key && search == root.left?.key){
                console.log(`${prefix_left}${render_space(space_left)}${seperator_left}\x1b[32m${root.left?.key}\x1b[0m`)
            }else{
                if(root.left?.key) console.log(`${prefix_left}${render_space(space_left)}${seperator_left}${root.left?.key}`)
            }
            
            if(root.left?.key != null){
                render_tree(root.left, index+1)
            }
            if(root.right?.key && search == root.right?.key){
                console.log(`${prefix_right}${render_space(space_right)}${seperator_right}\x1b[32m${root.right?.key}\x1b[0m`)
            }else{
                if(root.right?.key) console.log(`${prefix_right}${render_space(space_right)}${seperator_right}${root.right?.key}`)
            }
            if(root.right?.key != null){
                render_tree(root.right, index+1)
            }
        }

        console.log('\n🖥️  Tree View')
        console.log('---------------------------------')
        render_tree(this.root, this.index)
        console.log('')
        console.log('---------------------------------\n')
    }

    searchProcess(key){
        const binarySearch = (root, key) => {
            if(root == null || root.key == key){
                return root
            }

            // console.log(root.key)
            if(root.key < key){
                return binarySearch(root.right, key)
            }else{
                return binarySearch(root.left, key)
            }
        }

        console.log('')
        console.log('---------------------------------')
        console.log(`🔎 Searching data with key ${key}`);
        let found = binarySearch(this.root, key);

        if(found) {
            console.log('\x1b[32m')
            console.log(`✔️ Data found \n`)
            console.log(JSON.stringify(found, null, 4))
            console.log('\x1b[0m')
            this.treeView(key);
        }else{
            console.log('\x1b[31m')
            console.log("❌ Data Not Found")
        }
        console.log('\x1b[0m')
        console.log('---------------------------------')

        const question = () => {
            rl.question("try with another key? (y/t) ", (answer) => {
                if(answer == 'y' || answer == 'Y'){
                    console.log('')
                    this.showQuestion()
                }else{
                    rl.close()
                }
            })
        }
        question()
    }

    showQuestion(){
        const question = () => {
            rl.question("Please enter key: ", (answer) => {
                answer = +answer
                let validate = (typeof answer == 'number') && !isNaN(answer) && answer != '';
                if(validate){
                    this.searchProcess(answer)
                }else{
                    console.log('\x1b[33m%s\x1b[0m', "🛈 Input must be number")
                    question()
                }
            })
        }

        question()
    }
}

let root = {
    key: 8,
    right: {
        key: 10,
        right: {
            key: 14,
            right: {
                key: 1
            },
            left: {
                key: 13,
            },
        },
        left: {
            key: 9
        }
    },
    left: {
        key: 3,
        right: {
            key: 6,
            right: {
                key: 7,
                right: {
                    key: null,
                },
                left: {
                    key: null
                }
            },
            left: {
                key: 4,
                right: {
                    key: null,
                },
                left: {
                    key: null
                }
            }
        },
        left: {
            key: 1,
            right: {
                key: null,
            },
            left: {
                key: null,
            }
        }
    }
}

let obj = new BinaryTree(root)
obj.treeView()
obj.showQuestion()